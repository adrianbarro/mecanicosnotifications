package com.example.notificationservicemecanicos;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import androidx.core.content.FileProvider;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Objects;

public class Updater {

    public static void update(Context context, String apk) {
        new UpdateTask(context, apk).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private static class UpdateTask extends AsyncTask<Object, Void, Boolean> {
        Context context;
        String apk;

        ProgressDialog d;

        public UpdateTask(Context context, String apk) {
            this.context = context;
            this.apk = apk;
        }

        @Override
        protected void onPreExecute() {
            d = new ProgressDialog(context);
            d.setMessage("Actualizando...");
            d.show();
        }

        protected Boolean doInBackground(Object... params) {
            try {
                return descargar(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                                + "/" + BuildConfig.APK_NAME,
                        apk);
            } catch (Exception e) {
                Log.v("tag", "Error: " + e.getMessage());
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean arg0) {
            d.dismiss();

            if (arg0) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                    intent.setDataAndType(FileProvider.getUriForFile(Objects.requireNonNull(context),
                            BuildConfig.APPLICATION_ID + ".provider",
                            new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) +
                                    "/" +
                                    BuildConfig.APK_NAME)),
                            "application/vnd.android.package-archive");

                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                context.startActivity(intent);
                }else {

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) +
                            "/" +
                            BuildConfig.APK_NAME)), "application/vnd.Android.package-archive");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    context.startActivity(intent);
                }
            } else {
                Toast.makeText(context.getApplicationContext(), "Ocurrió un error al actualizar", Toast.LENGTH_LONG).show();
            }
        }
    }

    public static boolean descargar(String ruta, String url) {
        final HttpClient client = new DefaultHttpClient();
        final HttpGet getRequest = new HttpGet(url);

        boolean dev = false;

        try {
            HttpResponse response = client.execute(getRequest);
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                Log.v("tag", "Error " + statusCode + " while retrieving bitmap from " + url);
                return false;
            }

            final HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream inputStream = null;
                try {

                    FileOutputStream fileOutput = new FileOutputStream(new File(ruta));
                    inputStream = entity.getContent();

                    byte[] buffer = new byte[1024];
                    int bufferLength = 0;
                    while ((bufferLength = inputStream.read(buffer)) > 0) {
                        fileOutput.write(buffer, 0, bufferLength);
                    }

                    fileOutput.close();
                    dev = true;
                } finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    entity.consumeContent();
                }
            }
        } catch (Exception e) {
            // Could provide a more explicit error message for IOException or IllegalStateException
            getRequest.abort();
            Log.v("tag", "Error while retrieving bitmap from " + url + ", " + e.toString());
            dev = false;
        } finally {
            if (client != null) {
               client.getConnectionManager().closeExpiredConnections();
            }
        }

        return dev;
    }
}