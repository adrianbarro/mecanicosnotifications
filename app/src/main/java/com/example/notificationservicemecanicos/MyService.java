package com.example.notificationservicemecanicos;

import static android.media.RingtoneManager.getDefaultUri;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.Browser;
import android.service.notification.StatusBarNotification;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public class MyService extends Service {



    public static final String BROADCAST_ACTION = "com.supun.broadcasttest";
    private static final String CHANNEL_ID = "22" ;
    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm ");
    private static  Boolean  iniciado= false;
    @SuppressLint("StaticFieldLeak")
    public static Context ctx = null;
    private Runnable runnable= null;
    Intent intent;
    boolean avisado = false;
    private Handler handler = new Handler();
    public static WebSocket webSocket;
    public static BroadcastReceiver broadcast;
    private OkHttpClient client;
    private Builder builder;
    private Request request;
    public NotificationManagerCompat mNotificationManager;
    public  NotificationManager nmNotificationManager;
    public SocketListener socketListener;
    public int MAX_INTENTOS_RECONEXION = 5000;
    public int intentosReconexion = MAX_INTENTOS_RECONEXION;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate() {
        super.onCreate();
        createNotificationChannel();
        mNotificationManager = NotificationManagerCompat.from(getApplicationContext());
        intent=new Intent(BROADCAST_ACTION);
        startForeground(-1, buildForegroundNotification(true));
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        @SuppressLint("InvalidWakeLockTag") PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakelockTag");

        wakeLock.acquire(10*60*1000L /*10 minutes*/);
        Log.w("conectify", "inicio...");
        instantiateWebSocket();
        broadcast = new BroadcastReceiver() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onReceive(Context context, Intent intent) {
                final Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getApplicationContext().getPackageName() + "/" + R.raw.sound);  //Here is FILE_NAME is the name of file that you want to play

                int requestID = (int) System.currentTimeMillis();

                Uri alarmSound = getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                Intent notificationIntent = new Intent(getApplicationContext(), MyService.class);

                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                Intent openUri = new Intent(Intent.ACTION_VIEW, intent.getStringExtra("tipo").equals("1") ?
                        Uri.parse(context.getString(R.string.SERVER_URL)):
                        Uri.parse(context.getString(R.string.SERVER_URL)+"?id_averia="+intent.getStringExtra("idAveria")));
                openUri.putExtra(Browser.EXTRA_APPLICATION_ID, context.getPackageName());
                PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), requestID,openUri, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_notifications_black_24dp)
                        .setContentTitle(intent.getStringExtra("tittle"))
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(intent.getStringExtra("body")))
                        .setVibrate(new long[] { 1000, 1000, 1000})
                        .setContentText(intent.getStringExtra("body")).setAutoCancel(true);
                mBuilder.setSound(sound);
                mBuilder.setContentIntent(contentIntent);

                mNotificationManager.notify(Integer.parseInt(intent.getStringExtra("idAveria")), mBuilder.build());

            }
        };
        getApplicationContext().registerReceiver(broadcast, new IntentFilter(MyService.BROADCAST_ACTION));

    }


    @Override
    public void onDestroy() {
        Log.d("destruir", "onDestroy: ");
        try {
            webSocket.close(1000, "nada");
        }catch (Exception e){
            Log.e("eeror",e.getMessage());
        }
        stopForeground(true);
        getApplicationContext().unregisterReceiver(broadcast);
        super.onDestroy();
    }

    private Notification buildForegroundNotification(boolean esNotificada) {

        String connectionId =  SharedPreference.getId(getApplicationContext());
        String socketState = iniciado ? "CONECTADO ID-> "+connectionId : "DESCONECTADO";
        RemoteViews view = iniciado ? new RemoteViews(getPackageName(),R.layout.conected_state): new RemoteViews(getPackageName(),R.layout.disconected_state);
        if(iniciado)  view.setTextViewText(R.id.connect_state,"CONECTADO ID-> "+connectionId);
        Intent inte = new Intent(getApplicationContext(),BroadcastReciver.class);
        PendingIntent pend = PendingIntent.getBroadcast(getApplicationContext(),0,inte,0);
        view.setOnClickPendingIntent(R.id.reconect_from_view,pend);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notifications_black_24dp)
                .setContentTitle("Servicio de notificaciones")
                .setCustomContentView(view)
                .setOnlyAlertOnce(esNotificada)// si es true no notifica
                .setVibrate(new long[] { 1000, 1000, 1000})
                .setColor(Color.GRAY)
                .setContentText(socketState).setAutoCancel(true);
        //mBuilder.setSound(sound);

        return mBuilder.build();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startForeground(-1, buildForegroundNotification(true));
        return START_STICKY;
    }

    public  void instantiateWebSocket() {
        Log.d("conectify", "instantiateWebSocket: " + webSocket);
        client = new OkHttpClient();
        socketListener = new SocketListener();
        client = new OkHttpClient.Builder()
                .pingInterval(10, TimeUnit.SECONDS)
                .protocols(Collections.singletonList(Protocol.HTTP_1_1))
                .build();

        request = new Request.Builder()
                .header("Connection", "keep-alive,Upgrade")
                .header("Upgrade", "websocket")
                .header("Sec-WebSocket-Version", "13")
                .url(getApplicationContext().getString(R.string.SOCKET_SERVER))
                .build();
        client.newWebSocket(request, socketListener);
        client.dispatcher().executorService().shutdown();
        client.connectionPool().evictAll();

    }


    public class SocketListener extends WebSocketListener {


        Handler handler =new Handler();


        @Override
        public void onOpen(final WebSocket webSocket1, Response response) {

            Log.d("conectify", "run: conectado");

            handler.post(new Runnable() {

                @Override
                public void run() {
                    try {
                        while(webSocket != null) {
                            try {
                                webSocket.close(1000, "nada");
                                webSocket = null;
                            } catch (Exception e) {
                                Log.e("eeror", e.getMessage());
                            }
                        }
                        webSocket = webSocket1;
                        iniciado = true;
                        mNotificationManager.notify(-1, buildForegroundNotification(true));
                        avisado = false;//Variable que controla si la notificacion de desconectado ya ha sido lanzada se pone a true la primera vez que entra en on failure
                        intentosReconexion = MAX_INTENTOS_RECONEXION;
                    }catch (Exception e){
                        Log.e("tag3", "run: "+e);
                        Log.d("conectify", "run: conectado");
                    }

                }

            });

        }


        @Override
        public void onMessage(WebSocket webSocket, final String text) {

            handler.post(new Runnable() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void run() {

                    Log.d("mensaje2", "run: "+text);
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(text);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {

                        deleteNotificationsEnqueue();

                        if (jsonObject != null) {
                            Log.d("mensaje3", "run: "+jsonObject.getJSONObject("data").getString("idAveria"));
                            intent.putExtra("body",jsonObject.getString("body"));
                            intent.putExtra("tittle",jsonObject.getString("title"));
                            intent.putExtra("idAveria",jsonObject.getJSONObject("data").getString("idAveria"));
                            intent.putExtra("tipo",jsonObject.getJSONObject("data").getString("tipo"));

                           switch (jsonObject.getJSONObject("data").getString("tipo")){
                               case "2":
                                   SharedPreference.setId(jsonObject.getJSONObject("data").getString("idAveria"),getApplicationContext());
                                   if(MainActivity.activityVisible){
                                        MainActivity.id.setText(jsonObject.getJSONObject("data").getString("idAveria"));
                                   }
                                   mNotificationManager.notify(-1, buildForegroundNotification(true));
                                   break;
                               case "1":
                                   sendBroadcast(intent);
                                   break;
                               default:
                                   String nave = getResources().getStringArray(R.array.naves)[SharedPreference.getNave(getApplicationContext())].toUpperCase();
                                    if(jsonObject.getJSONObject("data").getString("nave").substring(jsonObject.getJSONObject("data").getString("nave").length()-1).equals(nave.substring(nave.length() -1))) {
                                        sendBroadcast(intent);
                                    }
                                   break;
                           }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }catch (Exception e){
                        Log.e("Error ", e.getMessage());
                    }
                }
            });
        }



        @Override
        public void onMessage(WebSocket webSocket, ByteString bytes) {
            super.onMessage(webSocket, bytes);
        }

        @Override
        public void onClosing(WebSocket webSocket, int code, String reason) {
            super.onClosing(webSocket, code, reason);
            webSocket.close(1000, null);
            Log.e("close", "e");
        }



        @Override
        public void onFailure(final WebSocket webSocket, final Throwable t, @Nullable final Response response) {
            super.onFailure(webSocket, t, response);

            handler.post(new Runnable() {

                @Override
                public void run() {

                    try {
                        iniciado = false;
                        SharedPreference.setId(null,ctx);
                        if(MainActivity.activityVisible){

                        }
                        Log.e("conectify", "run: no conectado "+t.getLocalizedMessage());

                        if(!avisado) {
                            mNotificationManager.notify(-1, buildForegroundNotification(true));
                            avisado = true;
                        }
                        if(intentosReconexion == 0) {
                            mNotificationManager.notify(-1, buildForegroundNotification(false));
                        }
                        intentosReconexion = intentosReconexion -1;

                        instantiateWebSocket();

                    }catch (Exception e){
                        Log.e("conectify", "run: error al conectar"+e.getLocalizedMessage());
                    }

                }

            });
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    //Eliminar las 10 primeras notificaciones cuando se supere el limite de 35
    private void deleteNotificationsEnqueue() {
        StatusBarNotification[] notifications = nmNotificationManager.getActiveNotifications();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            List<StatusBarNotification> notificationlist = Arrays.stream(notifications).sorted(Comparator.comparingLong(statusBarNotification ->  statusBarNotification.getPostTime())).collect(Collectors.toList());
            if(notifications.length >= 35){
                final int[] count = {0};
                notificationlist.forEach(statusBarNotification -> {
                    if(count[0] <= 10) {
                        if(statusBarNotification.getId() != -1) {
                            mNotificationManager.cancel(statusBarNotification.getId());
                            count[0]++;
                        }
                    }
                });
            }
        }
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getApplicationContext().getPackageName() + "/" + R.raw.sound);  //Here is FILE_NAME is the name of file that you want to play

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();

            channel.enableLights(true);
            channel.setLightColor(Color.MAGENTA);
            channel.enableVibration(true);
            channel.setSound(sound, attributes);
            nmNotificationManager = getSystemService(NotificationManager.class);
            if (nmNotificationManager != null) {
                nmNotificationManager.createNotificationChannel(channel);
            }
        }
    }



}