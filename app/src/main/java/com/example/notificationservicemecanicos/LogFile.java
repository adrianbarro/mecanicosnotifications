package com.example.notificationservicemecanicos;

import android.os.Build;
import android.os.Environment;

import androidx.annotation.RequiresApi;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class LogFile {

    public static String direccion = Environment.getExternalStorageDirectory()+"/log.txt";
    public static File fich = new File(direccion);

    public static void writeFile(Log l){
        try {
            BufferedWriter br = new BufferedWriter(new FileWriter(direccion, true));
            br.write(l.toString()+";");
            br.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void statusFile(){

        File[] all_files =  Environment.getExternalStorageDirectory().listFiles();

        Arrays.stream(all_files).filter(File::isFile).forEach(file -> {

            Long fecha = file.lastModified();

            Date fecha_mod = new Date();
            fecha_mod.setTime(fecha);

            Calendar fecha_5_day_ago  = Calendar.getInstance();
            fecha_5_day_ago.set(fecha_5_day_ago.get(Calendar.YEAR),fecha_5_day_ago.get(Calendar.MONTH),fecha_5_day_ago.get(Calendar.DATE) - 5);

            if(fecha_mod.before(fecha_5_day_ago.getTime())){
                android.util.Log.d("Para borrar", "Nombre: " + file.getName() + "\nFecha: " + fecha_mod.toString() + "---"+fecha_5_day_ago.getTime());
            }else {
                android.util.Log.d("Se mantiene", "Nombre: " + file.getName() + "\nFecha: " + fecha_mod.toString() + "---"+fecha_5_day_ago.getTime());
            }

        });


    }

    public static List<Log> readFile() {
        StringBuilder text = new StringBuilder();
        List<Log> loglist = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(direccion));
            String line;


            while ((line = br.readLine()) != null) {
                text.append(line);
            }

            br.close();

            loglist =  new Log().toListLog(text.toString());


        }
        catch (IOException e) {
            //You'll need to add proper error handling here
        }
        return loglist;
    }
}
