package com.example.notificationservicemecanicos;

import java.util.ArrayList;
import java.util.List;

public class Log {
    public String Description;
    public String fechacreacion;
    public Integer status;


    public Log(String description, String fechacreacion, Integer status) {
        Description = description;
        this.fechacreacion = fechacreacion;
        this.status = status;
    }

    public Log() {

    }




    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getFechacreacion() {
        return fechacreacion;
    }

    public void setFechacreacion(String fechacreacion) {
        this.fechacreacion = fechacreacion;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "{" +
                "Description='" + Description + '\'' +
                "||fechacreacion=" + fechacreacion +
                "|| status=" + status +
                '}';
    }
    
    public List<Log> toListLog(String listlog){
        
        String[] logs = listlog.split(";");
        List<Log> loglist = new ArrayList<>();

        for (String str: logs) {
           String[] params = str.split("\\|\\|");

           try {
               Log log = new Log();
               log.setDescription(params[0].split("=")[1]);
               log.setFechacreacion(params[1].split("=")[1]);
               log.setStatus(Integer.parseInt(params[2].split("=")[1].replace("}", "")));

               loglist.add(log);
           }catch (Exception e){
               android.util.Log.d("error", "toListLog: " + str);
           }

        }

        return loglist;

        
    };
}
