package com.example.notificationservicemecanicos;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

public class NavesAdapter extends BaseAdapter implements SpinnerAdapter {

    public String[] navesarray;
    public Activity activity;

    public NavesAdapter(String[] navesarray, Activity activity) {
        this.navesarray = navesarray;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return navesarray.length;
    }

    @Override
    public Object getItem(int position) {
        return navesarray;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = activity.getLayoutInflater().inflate(R.layout.item_row_nave, parent, false);

        TextView textView = (TextView) row.findViewById(R.id.text_nave);
        textView.setText(navesarray[position]);

        return row;
    }

}
