package com.example.notificationservicemecanicos;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.Vibrator;
import android.provider.Browser;
import android.provider.Settings;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

public class MainActivity extends AppCompatActivity {

    private static final int PERMISSIONS_REQUEST = 1;
     TextView console ;
     TextView version ;
     TextView fechaversion ;
     public static TextView id ;
     ScrollView scrollView ;
     Spinner spiner_nave;
     boolean esVisibleRequestBattery = false;
    PowerManager pm;
     public static View network_state;
    public static boolean activityVisible;

    ConnectivityManager connectivityManager;
    NetworkInfo networkInfo ;


    @SuppressLint("InvalidWakeLockTag")
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Declaracion de variables
        pm = (PowerManager) getSystemService(POWER_SERVICE);
        spiner_nave = findViewById(R.id.nave);
        version = findViewById(R.id.txt_version);
        fechaversion = findViewById(R.id.txt_fechaversion);
        id = findViewById(R.id.txt_id);
        network_state = findViewById(R.id.network);
        connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo =connectivityManager.getActiveNetworkInfo();
        checkPermissions();


    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onResume() {
        if(!esVisibleRequestBattery) {
            if (
                    ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                            pm.isIgnoringBatteryOptimizations(getPackageName())
            ) {
                reloadData();
            }
        }else{
            if(!pm.isIgnoringBatteryOptimizations(getPackageName())){
                finish();
            }else{
                reloadData();
            }
        }
        super.onResume();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onStop() {
        activityVisible = false;
        if (
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        &&
                        pm.isIgnoringBatteryOptimizations(getPackageName()) == true
        ) {
            finish();
        }
        super.onStop();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onPause() {
        activityVisible = false;
        if (
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                        pm.isIgnoringBatteryOptimizations(getPackageName()) == true
        ) {
            finish();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        if(requestCode == 2){
            esVisibleRequestBattery = true;
        }
        super.startActivityForResult(intent, requestCode);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void reloadData(){

        activityVisible = true;
        Vibrator v = (Vibrator) getApplicationContext().getSystemService(VIBRATOR_SERVICE);
        MyService.ctx = getApplicationContext();
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        getApplicationContext().registerReceiver(new BroadcastReciver(), intentFilter);
        stopService(new Intent(getApplicationContext(), MyService.class));
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.O)
            startForegroundService(new Intent(getApplicationContext(), MyService.class));
        else
            startService(new Intent(getApplicationContext(), MyService.class));
        fechaversion.setText(BuildConfig.FECHA);
        version.setText(BuildConfig.VERSION_NAME);
        id.setText(SharedPreference.getId(this));

        final NavesAdapter adapter = new NavesAdapter(getResources().getStringArray(R.array.naves),this);
        spiner_nave.setAdapter(adapter);
        spiner_nave.setSelection(SharedPreference.getNave(getApplicationContext()));
        spiner_nave.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               if(position != SharedPreference.getNave(getApplicationContext()))
                    showConfirmDialog(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    public void showConfirmDialog(int position){
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(getResources().getString(R.string.text_confirm_dialog))
                .setPositiveButton(getResources().getString(R.string.confirm_dialog_positive_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreference.setNave(position,getApplicationContext());
                        stopService(new Intent(getApplicationContext(), MyService.class));
                        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.O)
                            startForegroundService(new Intent(getApplicationContext(), MyService.class));
                        else
                            startService(new Intent(getApplicationContext(), MyService.class));

                    }
                })
                .setNegativeButton(getResources().getString(R.string.confirm_dialog_negative_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        spiner_nave.setSelection(SharedPreference.getNave(getApplicationContext()));
                    }
                })
                .setCancelable(false);

        AlertDialog alert = builder.create();
        alert.show();

    }


    public void openweb(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.SERVER_URL)));
        intent.putExtra(Browser.EXTRA_APPLICATION_ID, getPackageName());
        startActivity(intent);
    }


    public void update(View view) {
        new android.app.AlertDialog.Builder(MainActivity.this)
                .setMessage("┐Descargar ahora la ultima version disponible?")
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Updater.update(MainActivity.this,
                                BuildConfig.SERVER_DOWNLOAD_APK
                                        .concat(BuildConfig.APK_NAME));
                    }
                })
                .setNegativeButton("Mas tarde", null)
                .show();
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent();

                    if (!pm.isIgnoringBatteryOptimizations(getPackageName())){
                        intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                        intent.setData(Uri.parse("package:" + getPackageName()));
                        startActivityForResult(intent,2);
                    }else {
                        reloadData();
                    }
                    return;

                }else{
                    finish();
                }
            }
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    void checkPermissions() {

        if (
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                }, PERMISSIONS_REQUEST);
            }
        } else {
            Intent intent = new Intent();
            if (!pm.isIgnoringBatteryOptimizations(getPackageName())){
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent,2);
            }else {
                reloadData();
            }
        }

    }
}
